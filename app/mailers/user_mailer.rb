class UserMailer < ApplicationMailer
	default from: "noreply@madhav.com"

  def welcome_email(user)
    @user = user
    mail(to: @user.email, subject: 'Welcome')
  end
end
