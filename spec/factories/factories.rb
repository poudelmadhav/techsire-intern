FactoryBot.define do
  factory :user do
    name "Madhav Paudel"
    email  "poudelmadhav143@gmail.com"
    password "password"
    password_confirmation "password"
  end
end