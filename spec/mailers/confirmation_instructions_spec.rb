require 'rails_helper'


describe "DELIVER #mail" do
  it 'sends a confirmation email' do
    user = FactoryBot.build :user
    expect { user.save }.to change(Devise.mailer.deliveries, :count).by(1)
  end
end
